SHELL := /bin/bash

.PHONY: run
run:
	export FLASK_APP=manage.py && export FLASK_ENV=development && .env/bin/flask run
